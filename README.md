# Express + MongoDB Boilerplate

## Requirements

-   NodeJS
-   MongoDB

## How to run

-   Setup `.env` file following `.env.example`
-   `npm install`
-   `npm run dev`
